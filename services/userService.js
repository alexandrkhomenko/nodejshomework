const { UserRepository } = require('../repositories/userRepository');

class UserService {

    getAll() {
        const users = UserRepository.getAll();
        if (!users) {
            throw Error('Users not found')
        }
        return users;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            throw Error('User not found')
        }
        return item;
    }
    create(data){
        const newUser = UserRepository.create(data);
        if(!newUser){
            throw Error('Users not created')
        }
        return newUser

    }

    update(id, dataToUpdate) {

        let data = {};
        let dataDb = UserRepository.getOne(id);
        if (!dataDb) {
            throw Error('User not found')
        }
        let mapToUpdate = new Map(Object.entries(dataToUpdate));
        let mapDb = new Map(Object.entries(dataDb));

        for (let [key, value] of mapToUpdate.entries()) {
            if (mapDb.has(key) && value !== mapDb.get(key)) {
                data[key] = value
            }
        }
        const params = id.id;
        const userUpdated = UserRepository.update(params, data);
        if (!userUpdated) {
            throw Error("User not updated")
        }
        return userUpdated;
    }

    delete(id) {
        const user = UserRepository.getOne({id: id});
        if (!user) {
            throw Error("User not found");
        }

        UserRepository.delete(id);
        const userIsDelete = UserRepository.getOne({id: id})
        if (userIsDelete) {
            throw  Error("User not deleted")
        }

        return {message: "User deleted"}
    }
}

module.exports = new UserService();