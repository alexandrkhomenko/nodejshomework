const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {


    getAll(){
        const fighters = FighterRepository.getAll();
        if(!fighters){
            throw Error ('Fighters not found')
        }
        return fighters
    }


    getOne(id){
        const fighter = FighterRepository.getOne(id);
        if(!fighter){
            throw Error ('Fighter not found')
        }
        return fighter;
    }


    create(data) {
        data.health = 100;
        const fighter = FighterRepository.create(data);
        if (!fighter) {
            throw Error('Fighter not created')
        }
        return fighter
    }


    update(id, dataToUpdate){

        let data = {};
        let dataDb = FighterRepository.getOne(id);
        if(!dataDb){
            throw Error ('Fighter not found')
        }
        let mapToUpdate = new Map(Object.entries(dataToUpdate));
        let mapDb = new Map(Object.entries(dataDb));

        for (let [key, value] of mapToUpdate.entries()){
           if(mapDb.has(key) && value !== mapDb.get(key)){
                data[key] = value
           }
        }
        const fighterUpdate =  FighterRepository.update(id.id, data);
        if(!fighterUpdate){
            throw Error ('Fighter not updated')
        }
        return fighterUpdate
    }


    delete(id) {
        const fighter = FighterRepository.getOne({id: id});
        if (!fighter){
            throw Error('Fighter not found')
        }
        FighterRepository.delete(id);
        const fighterIsDeleted = FighterRepository.getOne({id: id});
        if (fighterIsDeleted){
            throw Error('Fighter not deleted')
        }
        const result = {message: "Fighter deleted"};
        return result
    }

}

module.exports = new FighterService();