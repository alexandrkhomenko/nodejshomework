const responseMiddleware = ( req, res, next) => {


    if (res.data){
        res.status(200).send(res.data)
    }
    if (res.err){
        let error = {
            error: true,
            message: res.err.message
        };
        res.status(404).send(error)
    }
    next()
};

exports.responseMiddleware = responseMiddleware;