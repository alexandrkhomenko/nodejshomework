const { fighter } = require('../models/fighter');
const { FighterRepository } = require('../repositories/fighterRepository');

const createFighterValid = (req, res, next) => {


    const RULES = {
        name: 'required',
        power: 'required|number|max:100|min:0',
        defense: 'required|number|min:1|max:10'
    };

    let data = req.body;
    let err = [];

    if(data.id){
        err.push('id is not allowed')
    }


    for (let [field, fieldRules] of Object.entries(RULES)){

        const value = data[field];

        const error = {
            field: field,
            errors: []
        };

        const splitRule = fieldRules.split('|');
        for (let rule of splitRule) {
            const ruleDetails = rule.split(':');
            switch (ruleDetails[0]) {
                case 'required': {
                    if (!value) {
                        error.errors.push(ruleDetails[0])
                    }
                    break
                }
                case 'number': {
                    if(typeof value !== 'number'){
                        error.errors.push(ruleDetails[0])
                    }
                    break
                }
                case 'min': {
                    if (value < ruleDetails[1]){
                        error.errors.push(`${ruleDetails[0]} ${ruleDetails[1]}`)
                    }
                    break
                }
                case 'max': {
                    if (value > ruleDetails[1]){
                        error.errors.push(`${ruleDetails[0]} ${ruleDetails[1]}`)
                    }
                }

            }
        }
        if (error.errors.length > 0 ){
            err.push(`${error.field} : ${error.errors}`)
        }
    }



    const mapCheck = new Map(Object.entries(fighter));
    const mapFront = new Map(Object.entries(data));

    for (let [key, value] of mapFront.entries()){
        if(!mapCheck.has(key)){
            err.push('data is not correct');
        }
    }

    if (err.length > 0){
        res.status(400).send(err)
    }else{
        next();
    }
};
















const updateFighterValid = (req, res, next) => {
    let data = req.body;
    let err = [];

    const RULES = {
        power: 'number|max:100|min:0',
        defense: 'number|min:1|max:10'
    };

    const fighterExist = FighterRepository.getOne(req.params);

    if (fighterExist){
        if(data.id){
            err.push('id is not allowed')
        }

        const mapCheckPut = new Map(Object.entries(fighter));
        const mapFrontPut = new Map(Object.entries(data));

        for (let [key, value] of mapFrontPut.entries()){
            if(!mapCheckPut.has(key)){
                err.push('data is not correct');
            }
        }


        for (let [field, fieldRules] of Object.entries(RULES)){

            const value = data[field];

            if(value){
                const error = {
                    field: field,
                    errors: []
                };

                const splitRule = fieldRules.split('|');
                for (let rule of splitRule) {
                    const ruleDetails = rule.split(':');
                    switch (ruleDetails[0]) {
                        case 'required': {
                            if (!value) {
                                error.errors.push(ruleDetails[0])
                            }
                            break
                        }
                        case 'number': {
                            if(typeof value !== 'number'){
                                error.errors.push(ruleDetails[0])
                            }
                            break
                        }
                        case 'max': {
                            if (value > ruleDetails[1]){
                                error.errors.push(`${ruleDetails[0]} ${ruleDetails[1]}`)
                            }
                            break
                        }
                        case 'min': {
                            if (value < ruleDetails[1]){
                                error.errors.push(`${ruleDetails[0]} ${ruleDetails[1]}`)
                            }
                            break
                        }

                    }
                }
                if (error.errors.length > 0 ){
                    err.push(`${error.field} : ${error.errors}`)
                }
            }
        }
    }

    if(!fighterExist){
        err.push('Fighter not found')
    }

    console.log(err);
    if (err.length > 0){
        res.status(400).send(err)
    }else{
        next();
    }
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;