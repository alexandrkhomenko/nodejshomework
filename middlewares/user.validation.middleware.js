const { user } = require('../models/user');
const { UserRepository } = require('../repositories/userRepository');


const createUserValid = (req, res, next) => {

    let data = req.body;
    let err = [];

    const RULES = {
        email: 'required|mail|unique',
        password: 'required|min:3',
        firstName: 'required',
        lastName: 'required',
        phoneNumber: 'required|phone|unique'
    };

    if(data.id){
        err.push('id is not allowed')
    }

    for (let [field, fieldRules] of Object.entries(RULES)){

        const value = data[field] || '';
        const error = {
            field: field,
            errors: []
        };

        const splitRule = fieldRules.split('|');
        for (let rule of splitRule) {
            const ruleDetails = rule.split(':');
            switch (ruleDetails[0]) {
                case 'required': {
                    if (!value) {
                        error.errors.push(ruleDetails[0])
                    }
                    break
                }
                case 'mail': {
                    if(!value.match(/^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/)){
                        error.errors.push(ruleDetails[0])
                    }
                    break
                }
                case 'phone':{
                    if(!value.match( /^(380|\+380)\d{9}$/ )){
                        error.errors.push(ruleDetails[0])
                    }
                    break
                }
                case 'unique': {
                    let search = {};
                    search[field] = value;
                    const uniq = UserRepository.getOne(search);
                    if (uniq) {
                        error.errors.push(ruleDetails[0])
                    }
                    break
                }
                case 'min': {
                    if (value.length < ruleDetails[1]){
                        error.errors.push(`${ruleDetails[0]} ${ruleDetails[1]}`)
                    }
                    break
                }
            }
        }
        if (error.errors.length > 0 ){
            err.push(`${error.field} : ${error.errors}`)
        }
    }

    const mapCheck = new Map(Object.entries(user));
    const mapFront = new Map(Object.entries(data));

    for (let [key, value] of mapFront.entries()){
        if(!mapCheck.has(key)){
            err.push('data is not correct');
        }
    }


    if (err.length > 0){
        res.status(400).send(err)
    }else{
        next();
    }


};

const updateUserValid = (req, res, next) => {

    let data = req.body;
    let err = [];

    const RULES = {
        email: 'mail|unique',
        password: 'min:3',
        phoneNumber: 'phone|unique'
    };


    const valueUser = UserRepository.getOne(req.params);

    if (valueUser){

        if(data.id){
            err.push('id is not allowed')
        }

        for (let [field, fieldRules] of Object.entries(RULES)){

            const value = data[field] || '';

            if (value){
                const error = {
                    field: field,
                    errors: []
                };

                const splitRule = fieldRules.split('|');
                for (let rule of splitRule) {
                    const ruleDetails = rule.split(':');
                    switch (ruleDetails[0]) {
                        case 'required': {
                            if (!value) {
                                error.errors.push(ruleDetails[0])
                            }
                            break
                        }
                        case 'mail': {
                            if(!value.match(/^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/)){
                                error.errors.push(ruleDetails[0])
                            }
                            break
                        }
                        case 'phone':{
                            if(!value.match( /^(380|\+380)\d{9}$/ )){
                                error.errors.push(ruleDetails[0])
                            }
                            break
                        }
                        case 'unique': {
                            let search = {};
                            search[field] = value;
                            const uniqValue = UserRepository.getOne(search);

                            // console.log('dataUserField', valueUser[field]);
                            // console.log('dataValue', value);
                            if (uniqValue) {
                                if (valueUser[field] !== uniqValue[field]){
                                    error.errors.push(ruleDetails[0])
                                }
                            }
                            break
                        }
                        case 'min': {
                            if (value.length < ruleDetails[1]){
                                error.errors.push(`${ruleDetails[0]} ${ruleDetails[1]}`)
                            }
                            break
                        }
                    }
                }
                if (error.errors.length > 0 ){
                    err.push(`${error.field} : ${error.errors}`)
                }
            }

        }
    }

    if(!valueUser){
        err.push('User not found')
    }

    const mapCheck = new Map(Object.entries(user));
    const mapFront = new Map(Object.entries(data));

    for (let [key, value] of mapFront.entries()){
        if(!mapCheck.has(key)){
            err.push(`${key} is not correct `);
        }
    }

    if (err.length > 0){
        res.status(400).send(err)
    }else{
        next();
    }

};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;