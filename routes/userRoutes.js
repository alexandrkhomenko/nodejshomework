const {Router} = require('express');
const UserService = require('../services/userService');
const {createUserValid, updateUserValid} = require('../middlewares/user.validation.middleware');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();


router.get(
    '/',
    function (req, res, next) {
        try {
            const data = UserService.getAll();
            res.data = data;
        } catch (error) {
            res.err = error;
        } finally {
            next()
        }
    },
    responseMiddleware
);


router.get(
    '/:id',
    function (req, res, next) {
        try {
            const data = UserService.search(req.params);
            res.data = data;
        } catch (error) {
            res.err = error;
        } finally {
            next()
        }

    },
    responseMiddleware
);


router.post(
    '/',
    createUserValid,
    function (req, res, next) {
        try {
            const data = UserService.create(req.body);
            res.data = data;
        } catch (error) {
            res.err = error;
        } finally {
            next()
        }
    },
    responseMiddleware
);


router.put(
    '/:id',
    updateUserValid,
    function (req, res, next) {
        try {
            const data = UserService.update(req.params, req.body);
            console.log(1111, data);
            res.data = data;
        } catch (error) {
            res.err = error;
        } finally {
            next()
        }
    },
    responseMiddleware
);


router.delete(
    '/:id',
    function (req, res, next) {
        try{
            const data = UserService.delete(req.params.id);
            res.data = data;
        }catch (error) {
            res.err = error;
        }finally {
            next()
        }
},
    responseMiddleware
);


module.exports = router;