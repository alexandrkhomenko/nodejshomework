const {Router} = require('express');
const FighterService = require('../services/fighterService');
const {responseMiddleware} = require('../middlewares/response.middleware');
const {createFighterValid, updateFighterValid} = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', function (req, res, next) {
    try {
        const data = FighterService.getAll();
        res.data = data;
    } catch (err) {
        data.err = err;
    } finally {
        next()
    }
}, responseMiddleware);


router.get('/:id', function (req, res, next) {
    try {
        const data = FighterService.getOne(req.params);
        console.log(data)
        res.data = data
    } catch (err) {
        res.err = err;
    } finally {
        next()
    }
}, responseMiddleware);


router.post(
    '/',
    createFighterValid,
    function (req, res, next) {
    try {
        const data = FighterService.create(req.body);
        res.data = data;
    } catch (error) {
        res.err = error;
    } finally {
        next()
    }
}, responseMiddleware);


router.put(
    '/:id',
    updateFighterValid,
    function (req, res, next) {
    try{
        const data = FighterService.update(req.params, req.body);
        res.data = data;
    }catch (error) {
        res.err = error
    }finally {
        next();
    }
}, responseMiddleware);


router.delete('/:id', function (req, res, next) {
    try{
        const data = FighterService.delete(req.params.id);
        res.data = data
    }catch (err) {
        res.err = err
    }finally {
        next()
    }
},
    responseMiddleware
);



module.exports = router;